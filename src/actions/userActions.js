import {FETCH_USERS,LOADING} from './types';
import DATA from '../random_users.json';

export const fetchUsers = () => dispatch => {
    console.log(DATA)
    dispatch({type:LOADING}); 
   
    setTimeout(() => {
        fetch('https://randomuser.me/api/?results=20')
        .then(res=>res.json())
        .then(users=>dispatch({
            type:FETCH_USERS,
            payload:users.results
        }))
        .catch(err=>{
            console.log(err)
        })
        
    }, 300);
    
}
