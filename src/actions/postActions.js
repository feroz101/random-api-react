import {FETCH_POSTS,NEW_POST,FETCHING_POSTS} from './types';


export const fetchPosts = () => dispatch => {
      dispatch({
          type:FETCHING_POSTS,
          payload:true
      })
    setTimeout(()=>{  
    fetch("https://jsonplaceholder.typicode.com/posts")
        .then(res => res.json())
        .then(posts => 
            dispatch({
                type: FETCH_POSTS,
                payload: posts,
            })
   ,
            dispatch({
                type:FETCHING_POSTS,
                payload:false
            })

            )
        .catch(err=>{
            console.log("error"+err)
        
            dispatch({
                type:FETCHING_POSTS,
                payload:false
            })
        })
    },3000);
   
}
export const createPost = (postData) => dispatch => {

    fetch('https://jsonplaceholder.typicode.com/posts',{
        method: 'POST',
        headers: {
            'content-type' : 'application/json'
        },
        body: JSON.stringify(postData)
    })
    .then(res => res.json())
    .then(post => dispatch({
        type: NEW_POST ,
        payload: post
    }));
    
};