import React from 'react';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="App-header">

<form className="form-signin col-sm-3 col-xs-12">
      <div className="text-center mb-4">
        <img className="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"/>
        <h1 className="h3 mb-3 font-weight-normal">Login</h1>
      </div>

      <div className="form-label-group">
        <input type="email" id="inputEmail" className="form-control" placeholder="Email address" required autofocus/>
        <label for="inputEmail">Email address</label>
      </div>

      <div className="form-label-group">
        <input type="password" id="inputPassword" className="form-control" placeholder="Password" required/>
        <label for="inputPassword">Password</label>
      </div>

      <div className="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"/> Remember me
        </label>
      </div>
      <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p className="mt-5 mb-3 text-muted text-center">&copy; 2019-2020</p>
    </form>

     </div>
        );
    }
}


export default Login;
