import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchPosts} from '../actions/postActions';
import Loader  from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import PostCard from '../components/posts/postCard';

class Posts extends Component {
    
    constructor(props){
        super(props)
        this.state={
            status:'block'
        }
    }

    componentWillMount(){
        this.props.fetchPosts();    
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.newPost){
            this.props.posts.unshift(nextProps.newPost);
        }
    }

    render() {
      
        const postItems = this.props.posts.map((post,index)  => (    
        <PostCard
            key={index}
            title={post.title}
            body={post.body}
        />
        )
        );
     
       
    return (
        <div className="App">
        <header className="App-header">
       {this.props.pending?
        <Loader
         type="Triangle"
         color="#53AF50"
         height={100}
         width={100}
         timeout={3000} //3 secs

      />
       :
       <React.Fragment>
        <div className="mt-8">Our Blogs</div>
          <div className="row card-wrapper">
            {postItems}
          </div>
        </React.Fragment> 
        }
       </header>
        </div>
        )
    
    }
}
Posts.propTypes = {
    fetchPosts: PropTypes.func.isRequired,
    posts: PropTypes.array.isRequired,
    newPost: PropTypes.object
}

const mapStateToProps = state => ({
    posts : state.postsReducer.items,
    newPost : state.postsReducer.item,
    pending: state.postsReducer.pending
   
});

export default connect(mapStateToProps, { fetchPosts })(Posts);