import React from 'react';
import { fetchUsers } from '../actions/userActions';
import { connect } from 'react-redux';
import { UserCard } from '../components';
import { Button, Container, Row, Col } from 'react-bootstrap';
import {Zoom } from 'react-reveal';

import DATA from '../random_users.json';

class Users extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            gender: 'all',
            city: 'all',
            term: '',
            cities: []
        };
    }

    componentDidMount() {
          this.props.fetchUsers();

    }

    changeGender = (event) => {

        this.setState({
            gender: event.target.value,
        })

    }

    changeCity = (event) => {
        this.setState({
            city: event.target.value,
        })

    }

    onChangeText = (event) => {
        this.setState({
            term: event.target.value
        })
    }


    render() {

        const nameBuilder = (first, last) => {
            return first + " " + last;
        }


        const filteredData = this.props.users.filter((user) => {

            if (this.state.city === 'all' && this.state.gender === 'all' && this.state.term === '') {
                //if all conditions are not applied    
                console.log("no condition used");
                
                return user
            }

            else if (this.state.city !== "all" || this.state.gender !== "all" || this.state.term !== '') {

                //if conditions applied any one

                if (this.state.city !== "all" && this.state.gender === "all" && this.state.term === '') {
                    //if only city condition applied
                    console.log("only city ");
                    
                    return user.location.city === this.state.city
                }
                else if (this.state.gender !== "all" && this.state.city === "all" && this.state.term === '') {
                    console.log("only gender");
                    
                    //if only gender condition applied
                    return user.gender === this.state.gender
                }
                else if (this.state.term !== '' && this.state.city === "all" && this.state.gender === "all") {
                    //if only name condition appplied
                    console.log("only name");
                    
                    return user.name.first.includes(this.state.term)
                }

                else if (this.state.term !== '' && this.state.city !== "all" && this.state.gender === "all") {
                    //if city and gender applied
                    console.log("city and gender");
                    
                    return user.name.first.includes(this.state.term) && user.location.city === this.state.city
                }

                else if (this.state.term !== '' && this.state.city === "all" && this.state.gender !== "all") {
                    //if gender and name applied
                    console.log("gender and name");
                    
                    return user.name.first.includes(this.state.term) && user.gender === this.state.gender
                }

                else if (this.state.term === '' && this.state.city !== "all" && this.state.gender !== "all") {
                    //if city and gender applied
                    console.log("city and gender");
                    
                    return user.location.city === this.state.city && user.gender === this.state.gender
                }

                else {
                    //if all applied
                    console.log("all conditions");
                    
                    return user.location.city === this.state.city && user.gender === this.state.gender && user.name.first.includes(this.state.term)

                }
            }
            return user;
        });


        const usersList = filteredData.map(user => {
            return <div key={user.email} style={{ display: 'inline-block' }}>
                <UserCard name={nameBuilder(user.name.first, user.name.last)} img={user.picture.medium} city={user.location.city} />
            </div>

        })
        setTimeout(() => {
            window.scrollTo(0, 0)
        }, 600);


        var show, hide;
        const loading = this.props.loading;
        if (!loading) {
            show = 'inline-block';
            hide = 'none';

        }
        else {
            show = 'none';
            hide = 'inline-block';
        }





        const distinctcities = [...new Set(this.props.users.map(user => user.location.city))]
        const cities = distinctcities.map((city, index) => {
            return <option key={index}>{city}</option>
        });


        return (


            <div className="App">
                <div className="App-header">
                    <div className="hero-section">

                        <div className="filter-section">

                            <Container style={{ padding: 30 }}>
                                <Row style={{ justifyContent: 'center' }}>
                                    <Col sm={3} xs={12}>
                                        <div className="form-group">

                                            <label htmlFor="sel1">SEARCH</label>
                                            <input type="text" className="form-control" placeholder="search user by name" onChange={(e) => this.onChangeText(e)} id="usr" style={{ borderRadius: '0 0' }} />

                                        </div>
                                    </Col>

                                    <Col sm={{ span: 3 }} xs={6}>
                                        <div>

                                            <label htmlFor="sel1">GENDER</label>
                                            <select className="form-control" value={this.state.gender} onChange={this.changeGender} style={{ borderRadius: '0 0' }} >
                                                <option>all</option>
                                                <option>male</option>
                                                <option>female</option>

                                            </select>

                                        </div>

                                    </Col>
                                    <Col sm={{ span: 3 }} xs={6}>
                                        <div >

                                            <label htmlFor="sel1">CITY</label>
                                            <select className="form-control" value={this.state.city} onChange={this.changeCity} style={{ borderRadius: '0 0' }} >
                                                <option>all</option>
                                                {cities}
                                            </select>

                                        </div>

                                    </Col>

                                </Row>

                                <Col style={{justifyContent:'center',padding:30}}><span style={{fontWeight:'bold'}}>Showing {filteredData.length} Results out of {this.props.users.length}</span></Col>
                               
                            </Container>


                        </div>

                        {usersList}

                    </div>

                    <Button className="load-more-btn" variant="outline-danger" onClick={this.props.fetchUsers}><span style={{ display: `${hide}` }}>LOAD MORE</span><span style={{ display: `${show}` }}>LOADING</span><span className="spinner-grow spinner-grow-sm" style={{ display: `${show}` }}></span></Button>

                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => ({
     users: state.usersReducer.users,
    //users: DATA.results,
    loading: state.usersReducer.loading
})

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers: () => dispatch(fetchUsers())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Users);


// const cities = [...new Set(this.props.users.map(user => user.location.city))];
// this.setState({
//     cities: cities
// })
// console.log(cities)
