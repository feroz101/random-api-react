import React from 'react';
import {  Notification } from '../components';
import { ReactComponent as Rocket } from '../assets/startup.svg';
import { ReactComponent as Cloud } from '../assets/clouds.svg';
import { Button, Row, Container } from 'react-bootstrap';

class Welcome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          isShow: false,
        }
      }
    
      componentDidMount() {
    
      }
    
      render() {
    
        return (
     <div className="App">
            <Notification show={this.state.isShow} />
            
            <header className="App-header">
              <Cloud />
              <Cloud />
              <Rocket />
              <Cloud />
              <Cloud />
              <div className="hero-section">
                <p>Or lipsum as it is sometimes known, is dummy text used in laying out print,<br />
                  ypesetter in the 15th century who is thought to have scrambled
             </p>
    
                <Button variant="outline-success">TRI IT NOW</Button>
                <Button variant="outline-info">EXPLORE IT</Button>
              </div>
             <Container>
              <Row>
                {this.state.pictures}
              </Row>
              </Container>
            </header>
           
          </div>
    
        );
      }
    }
    
    

export default Welcome;
