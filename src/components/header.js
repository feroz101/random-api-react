import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { connect } from 'react-redux';
import { EXPANDED } from '../actions/navButtonActions';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isScrolled: false,
            isSmallDevice: false
        };
    }


    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {
        var deviceWidth = window.innerWidth;

        console.log(deviceWidth)

        if (window.pageYOffset > 40) {

            this.setState({
                isScrolled: true
            })
        }
        else {
            this.setState({
                isScrolled: false
            })
        }


    }


    render() {
        const items = [{
            menu: "Home",
            url: "/"
        },
        {
            menu: "Users API",
            url: "/users"
        }, {
            menu: "Blogs API",
            url: "/blogs"
        }
        ];
        const menuItems = items.map((item) =>
            <Nav.Link href={item.url} key={item.menu}>{item.menu}</Nav.Link>
        );

        const scrollEfect = this.state.isScrolled ? 'navbar-bg' : '';

        const isExpanded = this.props.isExpanded ? 'fab fa-facebook' : '';

        return (

            <Navbar collapseOnSelect expand="lg" className={`${scrollEfect}`} fixed="top">
                <Navbar.Brand href="#home" >Random API </Navbar.Brand>
                <Navbar.Toggle style={{outline:'none'}} onClick={this.props.expanded} className={`${isExpanded}`} aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        {menuItems}
                    </Nav>
                    <Nav>
                        <Nav.Link href="#deets"><i className="fa fa-cart-plus"></i></Nav.Link>
                        <Nav.Link eventKey={2} href="/login">
                            Sign In
                 </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

const mapStateToProps = state => {
    return {
        isExpanded: state.isExpanded
    }
}
const mapDispatchToProps = dispatch => {
    return {
        expanded: () => dispatch(EXPANDED())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
