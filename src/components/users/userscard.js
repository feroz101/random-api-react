import React from 'react';
import {Card,Image, Button, Badge} from 'react-bootstrap';
import {Fade} from 'react-reveal';
const UserCard=({img,name,city})=>  {
    
        return (
          
          <div className="">
          <Fade>
          
            <div className="card-container">
             <Card className="user-card">
             <Card.Body className="card-body">
      
      <Image className="card-img" roundedCircle src={img}/> 
       <div className="card-data">
       <Card.Subtitle className="pointer card-title text-uppercase">{name}</Card.Subtitle>
       <Card.Text><i className="fas fa-map-marker-alt"></i> {city}</Card.Text> 
       <Badge pill variant="primary">
    ui/ux
  </Badge>
  <Badge pill variant="primary">
    java
  </Badge>
  <Badge pill variant="primary">
    +10
  </Badge>

       </div>
   </Card.Body>
   <Card.Footer><Button variant="outline-danger">VIEW PROFILE</Button>
  </Card.Footer>
</Card>
             </div>
             </Fade>
             </div>
        );
   
}


export default UserCard;
