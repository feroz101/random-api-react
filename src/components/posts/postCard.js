import React from "react";
import { Card } from "react-bootstrap";
import './postCards.scss';

class PostCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {key,title,body} = this.props
    let color='#';
    let badgeColor='#';
    let letters = '0123456789ABCDEF';
    
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
        badgeColor+=letters[Math.floor(Math.random() * 16)];
      }   
    
    return (
      <div className="col-sm-6 col-xs-12 col-lg-4" style={{ marginTop: 20 }}>
        <Card  key={key} className="col-12 p-0 crd">
          {/* <Card.Img src="https://images.pexels.com/photos/1480520/pexels-photo-1480520.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" /> */}
          <div className="color-pallate" style={{backgroundColor:color,boxShadow:'2px 2px 20px 2px'+color+''}}><span className="color-name">{title}</span>
          <span class="badge badge-pill color-badge" style={{background:badgeColor}}>{color}</span>
          </div>
          <Card.Body className="post-body">
            {/* <Card.Title className="post-txt">{title}</Card.Title> */}
            <Card.Text className="post-txt">
             {body}
            </Card.Text>
          </Card.Body>
          <Card.Footer>foteer</Card.Footer>
        </Card>
      </div>
    );
  }
}

export default PostCard;
