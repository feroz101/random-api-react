import React from 'react';
import {Toast} from 'react-bootstrap';
const Notification =({show})=>{
return(
        <Toast  className="toast" show={show} delay={4000} style={{position:'fixed',top:80,right:10}} >
            <Toast.Header>
              <img
                src="holder.js/20x20?text=%20"
                className="rounded mr-2"
                alt=""
              />
              <strong className="mr-auto">Bootstrap</strong>
              <small>11 mins ago</small>
            </Toast.Header>
            <Toast.Body>
              Woohoo, you're reading this text in a Toast!
            </Toast.Body>
          </Toast>     
 )
}
export default Notification;