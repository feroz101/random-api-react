import React, { Component } from "react";
import { Navbar, Container, Row, Col, Image, Nav, Button, FormControl, InputGroup } from "react-bootstrap";
import { Img1, Img2, Img3, Img4, Img5, Img6 } from "../assets";
class Footer extends Component {
  render() {
    const parteners = [Img1, Img2, Img3, Img4, Img5, Img6];
    const icons = parteners.map(icon => <Image className="partner-logo box" key={icon.toString()} src={icon} />);
    return (
      <div className="overlay">
        <Container className="footer-container" fluid>
          {/*First Row Section */}
          <Row className="footer-row footer-row-1">
            <Col xs={12} sm={4}>
              SIGN UP FOR OUR EMAIL UPDATES
              <Col sm={12}>
                <p>
                  {" "}
                  Stay Up To Date with our newsletter,
                  <br />
                  articles,webnars and many more
                </p>
              </Col>
              <Col sm={{ span: 10, offset: 1 }} style={{ paddingBottom: "40px" }}>
                <InputGroup>
                  <FormControl type="text" />
                  <span className="input-group-btn">
                    <Button variant="danger">SUBSCRIBE</Button>
                  </span>
                </InputGroup>
              </Col>
            </Col>

            <Col xs={12} sm={4}>
              STAY CONNECTED WITH RANDOM API
              <Col className="social">
                <i className="fa fa-facebook" />
                <i className="fa fa-twitter" />
                <i className="fa fa-google" />
                <i className="fa fa-instagram" />
                <i className="fa fa-youtube" />
                <i className="fa fa-linkedin" />
                <i className="fa fa-rss-square" />
              </Col>
            </Col>
            <Col xs={12} sm={4} className="box-wrap">
              AFFILIATION AND ACKNOWLEDGEMENTS
              <Col>{icons}</Col>
            </Col>
          </Row>

          {/*Second Row Section */}

          <Row className="footer-row footer-row-2">
            <hr />
            <Col xs={12} sm={2}>
              <Navbar.Brand href="#home" className="brand float-right">
                Random API<span>&reg;</span>
              </Navbar.Brand>
            </Col>
            <Col xs={12} sm={10}>
              <Nav className="float-right">
                <Nav.Link>Portfolio</Nav.Link>
                <Nav.Link>Services</Nav.Link>
                <Nav.Link>Resources</Nav.Link>
                <Nav.Link>Auth</Nav.Link>

                <Nav.Link>Profile</Nav.Link>
              </Nav>
            </Col>
            <hr />
          </Row>

          <Row>
            <Col xs={12} sm={6}>
              <Nav>
                <Nav.Link>Copyright Notifications</Nav.Link>
                <Nav.Link>Privacy Policy</Nav.Link>
              </Nav>
            </Col>

            <Col xs={12} sm={6}>
              <span className="float-right">&copy; 2019 Random API, All Rights Reserved.</span>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Footer;
