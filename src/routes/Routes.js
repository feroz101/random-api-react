import React from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import {Welcome,Login,Posts,Users} from '../pages';
import {Header,Footer} from '../components';
class Routes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div>
            <Header />
            <Router>
                <Switch>
                   <Route path="/" component={Welcome} exact></Route>
                    <Route path="/login" component={Login} exact></Route>
                    <Route path="/blogs" component={Posts} exact></Route>
                    <Route path="/users" component={Users} exact></Route>
                </Switch>
            </Router>
            <Footer />
            </div> 
        );
    }
}


export default Routes;
