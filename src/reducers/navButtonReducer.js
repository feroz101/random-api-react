
const initialState={
    isExpanded: false
}
export default function (state={initialState}, action) {
    switch (action.type) {

        case 'EXPANDED':
            return {
                isExpanded: !state.isExpanded
            };

        default:
            return state

    }
}