
import { FETCH_USERS, LOADING } from '../actions/types';

const initialState = {
    users: [],
    loading:false
}

export default function (state = initialState, action) {

    switch (action.type) {
        case FETCH_USERS:
            return {
                ...state,
                users: action.payload,
                loading:true

            }

            case LOADING:
                return {
                    ...state,
                    loading:false
                }

        default:
            return state
    }
}