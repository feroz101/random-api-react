import { FETCH_POSTS, NEW_POST, FETCHING_POSTS } from '../actions/types';

const initialState = {
    items: [],
    item: {},
    pending: true
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS:
            return {
                ...state,
                items: action.payload,
                
            };
        case NEW_POST:
            return {
                ...state,
                item: action.payload,
               
            };
        case FETCHING_POSTS:
            return {
                ...state,
                pending:action.payload
            }    
        default:
            return state;
    }
}