
import {combineReducers} from 'redux';
import navButtonReducer from  './navButtonReducer';
import usersReducer from './usersReducer';
import postsReducer from './postsReducer';

export default combineReducers({
navBtnReducer:navButtonReducer,
usersReducer:usersReducer,
postsReducer:postsReducer
}) 

