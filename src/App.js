import React from 'react';
import Routes from './routes/Routes';
import { Provider } from 'react-redux';
import Store from './store';
import {Offline,Online} from 'react-detect-offline';
import Card from './components/posts/postCard';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './App.scss';
import './components/styles'
class App extends React.Component {

  render() {
    return (
      <div>
   <Offline>OOps! Your are offline</Offline>
   <Online>
      <Provider store={Store}>
        <Routes>
          <div>app</div>
        </Routes>
      </Provider>
    {/* <Card/> */}
      </Online>
  </div>  
   );
  }
}


export default App;
